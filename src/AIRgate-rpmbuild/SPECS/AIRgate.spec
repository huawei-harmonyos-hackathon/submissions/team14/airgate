Name:           AIRgate
Version:        1.0.0
Release:        1%{?dist}
Summary:        automatic gate opener

License:        0
URL:            0
Source0:        %{name}-%{version}.tar.xz

BuildRequires: java-11-openjdk,tar,rpmdevtools,rpmlint
Requires: java-11-openjdk

BuildArch: noarch

%description
automatic gate opener

%prep
%setup -q -n %{name}

%build
javac AIRgate.java  -cp %{_builddir}/%{name}/:/usr/share/java/pi4j/*

%install
rm -rf $RPM_BUILD_ROOT
mkdir -v -p %{buildroot}/%{_bindir}/%{name}
install -d %{buildroot}/usr/share/licenses/%{name}
install -d %{buildroot}/usr/share/doc/%{name}
cp -p -v %{_builddir}/%{name}/* %{buildroot}/%{_bindir}/%{name}
install %{_builddir}/%{name}/LICENSE %{buildroot}/usr/share/licenses/%{name}
install %{_builddir}/%{name}/README.md %{buildroot}/usr/share/doc/%{name}

%files
%license LICENSE
%doc README.md
%{_bindir}/%{name}

