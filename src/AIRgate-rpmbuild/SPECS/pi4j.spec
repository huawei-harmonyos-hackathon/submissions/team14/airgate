Name:           pi4j
Version:        1.2
Release:        1%{?dist}
Summary:        java library for raspberry pi
BuildArch:      noarch

License:        GNU aGPL
URL:            https://pi4j.com
Source0:        https://pi4j.com/download/%{name}-%{version}.zip

BuildRequires: java-11-openjdk,gzip,rpmdevtools,rpmlint 
Requires:      java-11-openjdk

%global curl /bin/curl --location --fail --silent --output


%description


%prep
/bin/curl --location --fail --silent --output %{_sourcedir}/%{name}-%{version}.zip https://pi4j.com/download/%{name}-%{version}.zip
unzip -d %{_builddir} %{_sourcedir}/%{name}-%{version}


%install
rm -rf $RPM_BUILD_ROOT
mkdir -v -p %{buildroot}/usr/share/java/%{name}
mkdir -v -p %{buildroot}/usr/share/licenses/%{name}
mkdir -v -p %{buildroot}/usr/share/doc/%{name}
install -m 755 %{_builddir}/%{name}-%{version}/lib/* %{buildroot}/usr/share/java/%{name}
install -m 444 %{_builddir}/%{name}-%{version}/LICENSE.txt  %{buildroot}/usr/share/licenses/%{name}/LICENSE.txt
install -m 444 %{_builddir}/%{name}-%{version}/README.md  %{buildroot}/usr/share/doc/%{name}/README.md

%files
%license LICENSE.txt
%doc README.md
/usr/share/java/%{name}
