# AIRgate-rpmbuild
rpm build tree for AIRgate and dependencies

Packaging must be performed on a RPM-based distribution, such as Fedora or Red Hat. Make sure you have the required sofware, and otherwise install it:

`$ sudo dnf install -y rpmdevtools rpmlint`

Then create your file tree you need to build RPM packages:

`$ rpmdev-setuptree`

or copy the entire tree from the repository.
In the first case, you also need to obtain a copy of the source code for AIRgate to package, and a SPEC file for each of the packages.
The directory where you created the rpm build tree is indicated here as `$RPMROOT` (most probably the home directory) so that the rpmbuild directory is placed at `$RPMROOT/rpmbuild` (most probably `~/rpmbuild`)
Place `AIRgate.spec` and `pi4j.spec` in the `$RPMROOT/rpmbuild/SPECS` directory and `AIRgate-1.0.0.tar.xz` in the `$RPMROOT/rpmbuild/SOURCES` directory.
Now you can build each package by running `rpbuild -ba $RPMROOT/rpmbuild/SPECS/<name>.spec`
You can find the built RPM files in `$RPMROOT/RPMS/noarch`
