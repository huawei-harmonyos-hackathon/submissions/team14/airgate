/**
 *      HttpGet
 *
 *      Helper class to send HTTP or HTTPS GET requests
 *
 *      Written by: Michele Lizzit <michele@lizzit.it>, 8 Apr 2016
 *      Last update: 25 Nov 2021
 *      Version: 1.1
 *
 *      Copyright (C) 2021
 *      
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *      
 *          http://www.apache.org/licenses/LICENSE-2.0
 *      
 *      Unless required by applicable law or agreed to in writing, software
 *      distributed under the License is distributed on an "AS IS" BASIS,
 *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *      See the License for the specific language governing permissions and
 *      limitations under the License.
 *      
 *      @author Michele Lizzit <michele@lizzit.it>
 *      @version 1.1 25 Nov 2021
 */

import java.io.*;
import java.net.*;

public class HttpGet {
        /**
         * Fetch and print the result of a GET request
         * 
         * @param args Optional CLI arguments, value is ignored
         */
        public static void main(String[] args) {
                System.out.println(getHTML("https://example.com/"));
        }

        /**
         * Given an URL returns a string with the body of the HTTP response
         * 
         * @param urlString String containing the target URL of the request
         * @return Returns the response of the server or the "ERR" string in case of errors
         */
         public static String getHTML(String urlString) {
                try {
                        URL url = new URL(urlString);

                        HttpURLConnection conn = (HttpURLConnection)url.openConnection();

                        conn.setRequestMethod("GET");

                        BufferedReader rd = new BufferedReader(new InputStreamReader(conn.getInputStream()));
                        String line;
                        String result;
                        result = "";

                        while ((line = rd.readLine()) != null) {
                                result += line + "\n";
                        }

                        rd.close();

                        return result.toString();
                }
                catch (Exception e) {
                        return "ERR";
                }
         }
}
