/**
 *      TelegramBot
 *
 *      Helper class for using the Telegram Bot API
 *
 *      Written by: Michele Lizzit <michele@lizzit.it>, 23 Nov 2021
 *      Written by: Andrea Lizzit <andrea@lizzit.it>, 23 Nov 2021
 *      Last update: 25 Nov 2021
 *      Version: 1.3
 *
 *      Copyright (C) 2021
 *      
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *      
 *          http://www.apache.org/licenses/LICENSE-2.0
 *      
 *      Unless required by applicable law or agreed to in writing, software
 *      distributed under the License is distributed on an "AS IS" BASIS,
 *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *      See the License for the specific language governing permissions and
 *      limitations under the License.
 *      
 *      @author Michele Lizzit <michele@lizzit.it>
 *      @author Andrea Lizzit <andrea@lizzit.it>
 *      @version 1.3 25 Nov 2021
 */

import java.io.*;
import java.util.Date;
import java.text.SimpleDateFormat;
import java.text.DateFormat;
import java.nio.file.*;

public class TelegramBot {
        private String apiKey = "";
        private String adminChatID = "";
        private final String url = "api.telegram.org";
        private final String protocol = "https://";
        private int lastUpdateId = 0;
        private final static String logFile = "TelegramBot.log";
        private final static Path logPath = Paths.get(logFile);


        /**
         * Class constructor
         * 
         * @param newApiKey             Telegram API key
         * @param newAdminChatID        Admin chat ID
         */
        public TelegramBot(String newApiKey, String newAdminChatID) {
                checkJavaVersion(1.8);
                
                apiKey = newApiKey;
                adminChatID = newAdminChatID;
                System.out.println("MSG_TXT\tCHAT_ID\t\tUPDT_ID\t\tNAME\tTIME");
        }

        /**
         * Class constructor
         * 
         * @param newApiKey             Telegram API key
         */
        public TelegramBot(String newApiKey) {
                this(newApiKey, "");
        }


        /**
         * Ensure that the Java version is at least minVer, otherwise print an error message 
         * 
         * @param minVer                Minimum Java version to accept
         */
        public static void checkJavaVersion(double minVer) {
                Double version = Double.parseDouble(System.getProperty("java.specification.version"));
                if (version < minVer) {
                        System.out.println("This code needs to be run with Java version " + minVer + " or greater.");
                        System.exit(0);
                }
        }

        /**
         * Fetch incoming Telegram messages and updates
         * 
         * @param result                String Array to fill with the update
         */
        public int getUpdate(String[] result) {
                String request = protocol + url + "/bot" + apiKey + "/getUpdates?offset=" + (lastUpdateId + 1) + "&timeout=1";
                String response = HttpGet.getHTML(request);
                if (response.contains("ERR")) return -2;
                if (!response.contains("update_id")) return -1;

                int updIDindex = response.indexOf("update_id");
                String strUpdateId = subStr(updIDindex + 11, ',', response);
                lastUpdateId = Integer.parseInt(strUpdateId);
                result[2] = strUpdateId;
                
                int chatIDindex = response.indexOf("chat\":{\"id\"");
                String strChatId = subStr(chatIDindex + 12, ',', response);
                int chatId = Integer.parseInt(strChatId);
                result[1] = strChatId;

                int textIndex = response.indexOf("text");
                String msgTxt = subStr(textIndex + 7, '"', response);
                result[0] = msgTxt;

                int firstNameIndex = response.indexOf("first_name");
                String firstName = subStr(firstNameIndex + 13, '"', response);
                result[3] = firstName;

                int lastNameIndex = response.indexOf("last_name");
                String lastName = subStr(firstNameIndex + 12, '"', response);
                result[4] = lastName;

                int timeIndex = response.indexOf("},\"date\":");
                String time = subStr(timeIndex + 12, ',', response);
                result[5] = parseTime(time);

                System.out.println(toString(result));
                sendMsg(adminChatID, toString(result), true);
                //botLog(toString(result));

                return 0;
        }

        /**
         * Send a Telegram message
         * 
         * @param chatId                ID chat of the recipient
         * @param msgTxt                Message content to send
         */
        public void sendMsg(String chatId, String msgTxt) {
                System.out.println(msgTxt + "\t" + chatId + "\tSENT\t\tSENT\tSENT");
                //botLog(toString(msgTxt + "\t" + chatId + "\tSENT\t\tSENT\tSENT"));

                sendMsg(chatId, msgTxt, true);

                sendMsg(adminChatID, msgTxt + "\t" + chatId + "\tSENT\t\tSENT\tSENT", true);
        }

        /**
         * Send a Telegram message without saving it the log file and log group
         * 
         * @param chatId                Chat ID of the recipient
         * @param msgTxt                Text of the message to send
         * @param noLog                 Value is ignored
         */
        public void sendMsg(String chatId, String msgTxt, boolean noLog) {
                String request = protocol + url + "/bot" + apiKey + "/sendMessage?chat_id=" + chatId + "&text=" + msgTxt;
                HttpGet.getHTML(request);
        }

        /**
         * Given a String, a starting position and an ending position 
         * returns the substring between the initial and end position
         *
         * @param startPos              Starting position
         * @param endChar               Ending position
         * @param input                 String to manipulate
         * @return                      Resulting substring
         */
        private static String subStr(int startPos, char endChar, String input) {
                String result;
                result = "";
                for (int cnt = startPos; (input.charAt(cnt) != endChar) && (cnt + 1 < input.length()); cnt++)
                        result += input.charAt(cnt);
                return result;
        }

        /**
         * Return a String in format %H:%M:%S from a UNIX timestamp
         * 
         * @param unixInt               Time in seconds since UNIX epoch
         * @return                      String formatted %H:%M:%S
         */
        private static String parseTime(int unixInt) {
                unixInt = unixInt % (3600 * 24);
                String result = "";

                result += unixInt / 3600;
                result += ":";
                unixInt = unixInt % 3600;
                result += unixInt / 60;
                result += ":";
                unixInt = unixInt % 60;
                result += unixInt;

                return result;
        }

        /**
         * Return a String formatted %H:%M:%S from a UNIX timestamp
         * 
         * @param unixString            Time in seconds since UNIX epoch
         * @return                      String formatted %H:%M:%S
         */
        private static String parseTime(String unixString) {
                String result = "";

                int unixInt = Integer.parseInt(unixString);

                result = parseTime(unixInt);

                return result;
        }

        /**
         * Convert a String array to a String
         * 
         * @param array                 String array
         * @return                      Formatted String of the input array
         */
        private static String toString(String[] array) {
                String result = "";

                for (int cnt = 0; cnt + 2 < array.length; cnt++)
                        result += array[cnt] + "\t";
                result += array[array.length - 1];

                return result;
        }

        /**
         * Append the input string to the log file
         * 
         * @param input                 String to log
         */
        private static void botLog(String input) {
                input = "TelegramBot: " + getDateTime() + " " + input + "\n";
                try {
                Files.write(logPath, input.getBytes(), StandardOpenOption.APPEND);
                } catch (IOException e) {
                        e.printStackTrace();
                }
        }

        /**
         * Return a String containing the current timestamp in "dd/MM/yy HH:mm:ss" format
         * 
         * @return                      String with the current date in format dd/MM/yy HH:mm:ss
         */
        private static String getDateTime() {
                DateFormat df = new SimpleDateFormat("dd/MM/yy HH:mm:ss");
                Date date = new Date();
                return df.format(date);
        }
}
