/**
 *      AIRgate
 *
 *      AIRgate main file - Keyless entry system for pedestrian gates
 *
 *      Written by: Michele Lizzit <michele@lizzit.it>, 23 Nov 2021
 *      Written by: Andrea Lizzit <andrea@lizzit.it>, 23 Nov 2021
 *      Last update: 25 Nov 2021
 *      Version: 1.3
 *
 *      Copyright (C) 2021
 *      
 *      Licensed under the Apache License, Version 2.0 (the "License");
 *      you may not use this file except in compliance with the License.
 *      You may obtain a copy of the License at
 *      
 *          http://www.apache.org/licenses/LICENSE-2.0
 *      
 *      Unless required by applicable law or agreed to in writing, software
 *      distributed under the License is distributed on an "AS IS" BASIS,
 *      WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *      See the License for the specific language governing permissions and
 *      limitations under the License.
 *      
 *      @author Michele Lizzit <michele@lizzit.it>
 *      @author Andrea Lizzit <andrea@lizzit.it>
 *      @version 1.3 25 Nov 2021
 */

import java.io.*;
import java.net.*;
import java.util.Properties;
import javax.sound.sampled.*;

public class AIRgate {
	final static boolean DEBUG_RADIO = false;
	final static boolean ENABLE_ADVERTISE = false;

	private static boolean gateOpened = false;
	private static boolean flushRXbuffer = false;

	final static String PROP_FILENAME = "app.config";

	private static String staticAdvertise = "";
	private static String[] authKeys = {};
	private static String[] keyNames = {};
	private static String staticAck = "";
	private static String telegramApiKey = "";
	private static String chatID = "";
	private static String openScript = "";

	public static void main(String[] args) {
		// Load properties from app.config file
		loadProperties();

		System.out.println("AIRgate opening system");
		//sendTelegram("AIRgate opening system: started");
		init();
		if (DEBUG_RADIO) System.out.println("nRF24L01+ init completed");
		doSleep(200);
		if (DEBUG_RADIO) nRF24L01.printRegisters();
		if (DEBUG_RADIO) System.out.println("nRF24L01+ done dumping registers");
		nRF24L01.setRXMode();
		if (DEBUG_RADIO) System.out.println("nRF24L01+ RX mode set");
		boolean end = false;
		long prevTransmit = System.currentTimeMillis();
		while (end != true) {
			if (flushRXbuffer) {
				flushRXbuffer = false;
				nRF24L01.flushBuffers();
			}

			// Continuously broadcast "staticAdvertise"
			// Not really needed since AIRgate tags are never in receive mode
			long currentTime = System.currentTimeMillis();
			if ((currentTime - prevTransmit > 1000) && (ENABLE_ADVERTISE)) {
				sendAdvertise();
				nRF24L01.setRXMode();
				doSleep(2);
				prevTransmit = System.currentTimeMillis();
			}
			if (nRF24L01.packetAvail()) {
				if (DEBUG_RADIO) System.out.print("New data: ");
				String data = nRF24L01.getPacket();
				if (DEBUG_RADIO) System.out.println(data);
				parseData(data);
			}
			if (gateOpened) {
				gateOpened = false;
				prevTransmit = System.currentTimeMillis() + 60000;
				flushRXbuffer = true;
			}
			doSleep(100);
			if (DEBUG_RADIO) {
				doSleep(5000);
				nRF24L01.printRegisters();
			}
		}    
	}

	public static void loadProperties() {
		Properties prop = new Properties();
		try (FileInputStream fi = new FileInputStream(PROP_FILENAME)) {
		    prop.load(fi);
		} catch (IOException e) {
		    e.printStackTrace();
		}

		// String to broadcast (the gate name) (ignored by the tag, almost useless)
		staticAdvertise = prop.getProperty("app.staticAdvertise");

		// Script to run to perform the open action (usually calls an hass.io WebHook)
		openScript = prop.getProperty("app.openScript");

		// Authorized keys (the string transmitted by the tag)
		authKeys = prop.getProperty("app.authKeys").toString().split(",");

		// Key names (the key name is sent on Telegram)
		keyNames = prop.getProperty("app.keyNames").toString().split(",");

		// Ack string to be transmitted when the gate is opened (ignored by the tag)
		staticAck = prop.getProperty("app.staticAck");
	
		// Telegram API key
		telegramApiKey = prop.getProperty("app.telegramApiToken");

		// The Telegram chat ID; messages will be sent to this ID
		chatID = prop.getProperty("app.chatId");
	}

	public static ReceiveData nRF24L01;

	public static void init() {
		nRF24L01 = new ReceiveData();
		nRF24L01.start();
	}

	public static void sendTelegram(String msgToSend) {
		TelegramBot myBot = new TelegramBot(telegramApiKey, chatID);
                myBot.sendMsg(chatID, msgToSend, false);
  	}

	public static void parseData(String message) {
		//if (nearOk() && authOk(message)) { //alternative line that checks signal strength
		if (authOk(message)) {
			if ((authNum(message) < 0) || (authNum(message) >= keyNames.length)) {
				sendTelegram("Gate NOT opened; unauthorized key:  " + message);
			}
			else {
				gateOpened = true;

				//Not strictly required, standard AIRgate Tags ignore this
				//sendAck();

				try {
					Clip beepClip = beepSound();
					doGateOpen();
					while(beepClip.getMicrosecondLength() > beepClip.getMicrosecondPosition()) {
						doSleep(100);
					}
					beepClip.close();
				}
				catch (UnsupportedAudioFileException e) {
					doGateOpen();
					e.printStackTrace();
				}
				catch (LineUnavailableException e) {
					doGateOpen();
					e.printStackTrace();
				}
				catch (IOException e) {
					doGateOpen();
					e.printStackTrace();
				}
				sendTelegram("Gate opened; key: " + keyNames[authNum(message)]);
				flushRXbuffer = true;
				doSleep(10000);
			}
			sendTelegram("AIRGate opened; key " + keyNames[authNum(message)]);
		}
		else if (authOk(message)) 
			System.out.println("AUTH OK");
		else {
			System.err.println("Packet Syntax ERR");
			sendTelegram("AIRGate opening attempt with incorrect code");
			doSleep(5000);
		}

	}

	public static void doGateOpen() {
		// Run the gate opening action
		try {
			Process runDoGateOpen = Runtime.getRuntime().exec(openScript);
			runDoGateOpen.waitFor();
		}
		catch (IOException e) {
			e.printStackTrace();
		}
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}

	public static boolean nearOk() {
		if (nRF24L01.signalStrength() > 0) return true;
		return false;
	}

	public static boolean authOk(String message) {
		if (authNum(message) >= 0) return true;
		return false;
	}

	public static int authNum(String message) {
		return strInArr(message, authKeys);
	}

	public static void sendAdvertise() {
		sendPacket(staticAdvertise);
	}

	public static void sendAck() {
		sendPacket(staticAck);
	}

	public static void sendPacket(String packetContent) {
		nRF24L01.sendStringNRF(packetContent);
	}
	
	/*
	 * Audio files
	 */

	public static Clip beepSound() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
		AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("tone.wav"));
		Clip clip = AudioSystem.getClip();
		clip.open(audioIn);
		clip.start();
		return clip;
	}

	public static Clip openSound() throws UnsupportedAudioFileException, LineUnavailableException, IOException {
		AudioInputStream audioIn = AudioSystem.getAudioInputStream(new File("openSound.wav"));
		Clip clip = AudioSystem.getClip();
		clip.open(audioIn);
		clip.start();
		return clip;
	}

	/*
	 * Helper methods
	 */
	public static int strInArr(String str, String[] arr) {
		for (int cnt = 0; cnt < arr.length; cnt++)
			if (str.contains(arr[cnt]))
				return cnt;
		return -1;
	}

	public static boolean stringCompare(String string1, String string2, int num) {
		for (int cnt = 0; cnt < num; cnt++) 
			if (string1.charAt(cnt) != string2.charAt(cnt))
				return false;
		return true;
	}

	public static void doSleep(int ms) {
		try {
			Thread.sleep(ms);
		} 
		catch (InterruptedException e) {
			e.printStackTrace();
		}
	}
}
