#!/bin/sh

# Build the project from scratch at boot,
# you might want to consider shipping Java Bytecode when in production
javac -classpath .:classes:/opt/pi4j/lib/'*' AIRgate.java;

echo "Built";

# Run as root to ensure access to the SPI interface
sudo java -classpath .:class:/opt/pi4j/lib/'*' AIRgate
