/*  
 *  Keyless entry system - code for the RFID active tag
 *  Author: Michele Lizzit <michele@lizzit.it> - lizzit.it
 *  Author: Andrea Lizzit <andrea@lizzit.it> - andrea.lizzit.it
 *  v1.0 - 25/11/2021
 *   
 *  
 *  Copyright (C) 2021
 *  
 *  Licensed under the Apache License, Version 2.0 (the "License");
 *  you may not use this file except in compliance with the License.
 *  You may obtain a copy of the License at
 *  
 *      http://www.apache.org/licenses/LICENSE-2.0
 *  
 *  Unless required by applicable law or agreed to in writing, software
 *  distributed under the License is distributed on an "AS IS" BASIS,
 *  WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 *  See the License for the specific language governing permissions and
 *  limitations under the License.
 */

#include "Config.h"

#include "nRF24L01_mini.h"
#include <avr/power.h>
#include <avr/sleep.h>
#include <avr/wdt.h>
#include "LowPower.h"

#ifdef DEBUG
unsigned long timeOn;
#endif

#if !defined(__AVR_ATmega328P__)
  #error Must use an ATmega328P board
#endif

void setup() {
  //if serial debug is enabled initialize the serial port and send a character
  #ifdef DEBUG
    Serial.begin(115200);
    Serial.println("Initializing...");
    timeOn = micros();
  #endif

  delayMicroseconds(50000);
  
  initializeNRF24L01(); //initialize the nRF24L01+
  powerDownNRF(); //put the nRF24L01+ into power down mode (0.1uA power consumption)
  //powerSaveMode(); //for some strange and unknown reason uses more power instead of less

  //WARNING: if the battery is low, tension will drop and the code 
  //will hang here, resetting the MCU and triggering an infinite loop
  //that will rapidly discarge the remaining battery
  //TODO: prevent complete battery discarge

  /*powerDownNRF(); //put the nRF24L01+ into power down mode (0.1uA power consumption)
  enterSleepMode(); //put the MCU into sleep mode
  wakeUpRoutine(); //function called when exiting sleep*/

  //if debug blink is enabled set pin 13 to output
  #ifdef DEBUG_BLINK13
    pinMode(13, OUTPUT);
  #endif
}

void loop() {
   powerUpNRF(); //power up the nRF24L01+ from "power down" state
   delayMicroseconds(150); //time required for the nRF24L01+ to start up
   sendStringNRF(authKey, 10); //transmit the 10-byte packet
   powerDownNRF(); //put the nRF24L01+ into power down mode (0.1uA power consumption)
   
   enterSleepMode(); //put the MCU into sleep mode
   //now the MCU waits 2 seconds in sleep mode
   wakeUpRoutine(); //function called when exiting sleep
   
   #ifdef DEBUG //if serial debug enabled send a char on the serial port
     Serial.println("W");
     Serial.flush();
   #endif

   #ifdef DEBUG_BLINK13 //if debug blink enabled, send a high pulse on pin 13
     digitalWrite(13, HIGH);
     delay(250);
     digitalWrite(13, LOW);
   #endif
}

void enterSleepMode() {
  #ifdef DEBUG
    Serial.print("MCU was ACTIVE for ");
    Serial.print(micros() - timeOn);
    Serial.println(" uS");
  #endif
  //power_spi_disable();
  LowPower.powerDown(SLEEP_2S, ADC_OFF, BOD_OFF);  
}

void wakeUpRoutine() {
  //power_spi_enable();  
  #ifdef DEBUG
    timeOn = micros();
  #endif
}

void powerSaveMode() {
  //disable brown out detection
  noInterrupts();
  MCUCR = bit (BODS) | bit (BODSE);
  MCUCR = bit (BODS);
  interrupts();

  //duplicate of the code above
  sleep_bod_disable(); 

  //disable ADC
  power_adc_disable();

  //disable I2C
  power_twi_disable();

  //disables some timers
  power_timer0_disable();
  power_timer1_disable();
  power_timer2_disable();
}
