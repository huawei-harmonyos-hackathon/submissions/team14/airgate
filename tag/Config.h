// Uncomment to enable debug messages on the serial port
//#define DEBUG

// Uncomment to send an high pulse on pin 13 every time a packet is sent
//#define DEBUG_BLINK13


//BOARD CONFIGURATION

// Uncomment to use the PCB pin scheme
#define BOARD_PCB1
// Uncomment to use the "prototype version" pin scheme
//#define BOARD_PROTOTYPE

// The static packet to be transmitted
byte authKey[] = {'K', 'E', 'Y', 'I', 'D', '0', '0', '0', '0', '1'};
