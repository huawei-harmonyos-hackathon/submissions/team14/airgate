# AIRgate
![](https://slwhckr.ml/AIRgate/pictures/AIRgate_logo.svg)

AIRgate is a smart device that improves the access to the perimeters of one's home. It provides automation of the unlocking mechanism of the pedestrian gate, thereby facilitating the entrance of the owner and removing a nuisance that otherwise presents itself daily, every time access to the private property needs to be granted.

AIRgate consists of two components. The receiver is a device that is installed on the gate and is responsible for granting access in response to a successful authentication attempt. The tag is a miniaturized device which is kept by the user, and communicates with the receiver to provide authentication.

## Security considerations
Authentication is done by transmitting a code from the tag to the receiver, which is compared to a reference list and if correct allows the opening of the gate. Within this protocol, the message is not encrypted. This is a design feature which is consistent with its purported usage and fits into the greater design philosophy of software minimalism.

The AIRgate system provides authentication and automatic opening of one's home's pedestrian gate. It is indeed possible, with the appropriate instrumentation and by staying within a short distance, to perform replay attacks on the gate system; however, pedestrian gates are not meant to be a security measure to keep malicious people outside, since in most cases they can simply be trespassed by leaping over them. It is clear then that providing encryption would not only be useless but also detrimental, potentially increasing the length of the build, the size of the software, and the power consumption of the tag. If the device were responsible for granting access to a high-security area, much greater attention would have to be put into security (tags can be stolen, pincodes copied, even fingerprints replicated from high-resolution photos), but in this case and as a general principle, the most efficient, minimalistic solution is chosen.

## Receiver

The receiver is run by an embedded Linux board which runs a Yocto Linux image created with the purpose of running the receiver software. This configuration allows to have a very lightweight software stack optimized for the function of the receiver.

The Linux board is connected to an nRF24L01+ wireless module, which it uses to detect the presence of a tag, and integrates with an existing smart IoT gate solution, either via hass.io or a custom webhook. This allows the system to be easily integrated also with IFTTT and other custom solutions. The Linux board uses the SPI interface to communicate with the wireless module, therefore the presence of a SPI bus is a requirement of the target Linux board.

| ![Receiver close](https://slwhckr.ml/AIRgate/pictures/receiver_close.jpg) | ![Receiver](https://slwhckr.ml/AIRgate/pictures/receiver_far.jpg)
| --- | --- |
| The receiver unit (closeup) | The receiver unit |

| ![Receiver overview](https://slwhckr.ml/AIRgate/pictures/overview.jpg) |
| --- |
| Receiver unit installed on the gate |

A custom integration for the Telegram messaging application has been developed to further demonstrate the capabilities of the AIRgate system. When the gate is opened via authentication with an AIRgate tag, the receiver sends a telegram message to a chat which notifies the participants of the event. The chat can be configured by writing an `app.config` configuration file; a sample `app.config.sample` is provided in the AIRgate project directory and can be used as a template to create an `app.config` file.

The `app.config` configuration file also contains the tag identifier code which is kept as a reference to control whether to authenticate a request from a tag; it must therefore be edited in order to match the code of the tag firmware.

### Build instructions

A Raspberry PI was used for development and testing.  
A Yocto Linux image needs to be build for the board, by embedding in the image two RPM packages: `pi4j` and `AIRgate`. The first is a packaged version of a library for low-level interface with the Raspberry PI, while the second contains the logic for the functioning of the AIRgate receiver system.  

The reason why we chose Yocto is that it allows to effortlessly build the entire software stack for different platforms. Yocto provides a layer infrastructure which groups similar functionality and improves ease of customization and maintenance. Each layer is a set of metadata which is used to build a set of packages, and each layer can extend and add functionality to the previous ones.

The Yocto workflow starts with fetching the source code, extracting and unpacking it and applying patches to extend or customize functionality. Then comes a configuration step, after which packages are built, each according to a recipe, a file containing metadata which describes how to organize and build the source code. Finally, files are installed and packaged.  

The most simple way to build the Yocto image is to take a lightweight Yocto distribution and create a new layer (e.g. with `bitbake-layers create-layer AIRgate` and `bitbake-layers add-layer AIRgate`) and adding a recipe to embed and install the RPM files. Make sure the `packageformat` is set to `rpm`. The RPMs can be downloaded from the repository or repackaged directly from the most recent source code.  

This process allows development of the embedded product regardless of the chosen board's hardware architecture. Boards can be mass-produced and the software stack quickly adapted to each new hardware architecture, with the same end product.  

### Packaging instructions

Packaging must be performed on a RPM-based distribution, such as Fedora or Red Hat. Make sure you have the required sofware, and otherwise install it:

`$ sudo dnf install -y rpmdevtools rpmlint`

Then create your file tree you need to build RPM packages:

`$ rpmdev-setuptree`

or copy the entire tree from the repository.
In the first case, you also need to obtain from the repository a copy of the AIRgate source code and a SPEC file for each of the packages.
The directory where you created the rpm build tree is indicated here as `$RPMROOT` (most probably the home directory) so that the rpmbuild directory is placed at `$RPMROOT/rpmbuild` (most probably `~/rpmbuild`)

Place `AIRgate.spec` and `pi4j.spec` in the `$RPMROOT/rpmbuild/SPECS` directory and `AIRgate-1.0.0.tar.xz` in the `$RPMROOT/rpmbuild/SOURCES` directory.
Now you can build each package by running `rpbuild -ba $RPMROOT/rpmbuild/SPECS/<name>.spec`
You can find the built RPM files in `$RPMROOT/RPMS/noarch`

For convenience during development, we decided to ship an entire JDK within the Yocto image. During production, you might want to only include the JRE runtime and repackage the source code to compile it at the time of build.

## Tag

### Manufacturing instructions

Each tag is composed of an MCU and an nRF24L01+ wireless module. The module is then powered by a battery. As a design choice we decided to use an ATmega328P as MCU and a CR2032 as a battery. The ATmega328P is an AVR MCU that is well suited to ultra low-power use cases such as ours and is able to be powered on a wide range of voltages down to 1.8v.

The tag is built on a custom PCB, which allows to reduce the size of the tag and make it more portable. The PCB is manufactured according to the instructions present in the gerber files, which can be downloaded from the repository, together with the two files for copper etching.


| ![TAG](https://slwhckr.ml/AIRgate/pictures/tag1.jpg) | ![TAG](https://slwhckr.ml/AIRgate/pictures/tag4_res.jpg) |
|---|---|
| The tag before soldering on the nRF24L01+ | The complete tag |


The MCU is configured to use its internal (8MHz) oscillator in order to minimize the number of electronic components needed for a tag.  

Every two seconds the tag broadcasts a string and then puts the nRF24L01+ in "power down" mode; in this mode the nRF24L01+ draws about 100nA.  
The module draws 7.0mA while transmitting, but the total time the nRF24L01+ in not in power down mode is about 40ms every 2 seconds so the total power consumption is still very low.

Thanks to the minimal power consumption of the low-powered MCU, a small battery is sufficient to power the device, which is therefore very small and lightweight, and can be carried around by a person with no effort. Size and weight are very important for a device of this type: if the device were not miniaturized enough and a person notices its weight or bulk when carrying it around, or it could not be comfortably attached to one's backpack or fit into one's pockets, then independently from its features, it would be of little use and ultimately discarded.

| ![TAG ICSP](https://slwhckr.ml/AIRgate/pictures/tag_icsp.jpg) |
| --- |
| A tag connected to the AVR programmer through an ICSP connector |

The device is extremely energy-efficient, with an average power consumption of around 30µA. A single 210mAh CR2032 battery is sufficient to power it for over a year. This brings our tag in line with the current industry standards in the automotive sector, where each keyless car key can run for long periods of time with no battery replacement.

### Build instructions

The Atmega328P MCU runs a firmware that uses the nRF24L01+ wireless module to authenticate to the receiver.  

The firmware can be built for the AVR microcontroller and written through ICSP with an ISP. The source files can be downloaded from the repository. As a design choice, for ease of development, we depend on the Arduino libraries, therefore the Arduino IDE is required to build the project. However, if a smaller image footprint is required, the final C++ code can be very easily adapted to be compiled with `avr-gcc` with no external libraries.  

At boot the firmware disables all the unused features of the MCU in order to save power (all the timers, the I2C interface, the ADC and the BOD). Then the SPI bus is initialized and the wireless modules is configured to transmit at minimup power. Then both the MCU and wireless unit are set into sleep mode. Every 2S the MCU watchdog wakes up the MCU, which in turns wakes up the wireless unit and transmits a packet. Once the packet is transmitted (it takes about 40 µs) the wireless unit is again put into a power down state and the MCU enters sleep mode.  
Keeping the packet size small (10 bytes) allows us to reduce the time the wireless unit is kept into transmit mode and therefore reduce power consumption.  